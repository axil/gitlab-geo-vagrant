# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  # Disable the base shared folder
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.box = "ubuntu/xenial64"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.memory = "2048"

    # Fix possible error on macOS
    # https://github.com/deis/deis/issues/4360
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
  end

  config.vm.define "primary" do |primary|
    primary.vm.hostname = "primary.gitlab"
    primary.vm.network "private_network", ip: "192.168.42.10"
  end

  config.vm.define "secondary" do |secondary|
    secondary.vm.hostname = "secondary.gitlab"
    secondary.vm.network "private_network", ip: "192.168.42.20"
  end

  config.vm.provision "shell", inline: <<-SHELL
    # Add swap
    fallocate -l 2G /swapfile
    chmod 600 /swapfile
    mkswap /swapfile
    swapon /swapfile
    echo "/swapfile   none    swap    sw    0   0" | tee -a /etc/fstab
    echo "vm.swappiness=10" | tee -a /etc/sysctl.conf
    echo "vm.vfs_cache_pressure=50" | tee -a /etc/sysctl.conf
    # Update the system and install GitLab
    apt-get update
    apt-get -qq upgrade
    DEBIAN_FRONTEND=noninteractive apt-get install postfix
    apt-get install -y curl openssh-server ca-certificates
    curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
    apt-get install -y gitlab-ee=8.12.9-ee.0
    gitlab-ctl reconfigure
  SHELL

end
