# Simulate Geo deploy with Vagrant and VirtualBox

Easy setup of the infrastructure needed for Geo: https://docs.gitlab.com/ee/gitlab-geo/.

## Provision VMs

1. Install [Vagrant]
1. Install [VirtualBox]
1. Create a `vagrant` directory and clone this repo:

    ```
    mkdir vagrant
    cd vagrant
    git clone https://gitlab.com/axil/gitlab-geo-vagrant.git geo
    cd geo
    ```

1. Run `vagrant up`
1. Wait until all nodes are up
1. Edit your local `/etc/hosts` and add the following:

    ```
    192.168.42.10 primary.gitlab
    192.168.42.20 secondary.gitlab
    ```

1. Login in http://primary.gitlab and upload the EE license containing the Geo
   product
1. Follow the Geo setup documentation https://docs.gitlab.com/ee/gitlab-geo/

## Useful Vagrant commands

When inside the directory where `Vagrantfile` resides, you can use the commands
Vagrant provides:

| Command | Description |
| ------- | ----------- |
| `vagrant up <name>` | Start the VM defined. `<name>` can be either `primary` or `secondary`. If you omit `<name>`, then both machines will start. |
| `vagrant status <name>` | Learn about the status of the VM defined (running, halted, etc.). `<name>` can be either `primary` or `secondary`. If you omit `<name>`, then information for both machines will be shown. |
| `vagrant halt <name>` | Poweroff the VM defined. `<name>` can be either `primary` or `secondary`. If you omit `<name>`, then both machines will poweroff. |
| `vagrant destroy <name>` | Destroy the VM defined. `<name>` can be either `primary` or `secondary`. If you omit `<name>`, then both machines will be destroyed. |
| `vagrant ssh <name>` | SSH into the VM defined. `<name>` can be either `primary` or `secondary`.

## Default values

The `Vagrantfile` inside this repo provisions two identical VMs with 2GB of RAM
running Ubuntu 16.04 x86_64 on VirtualBox.

The primary Geo node has:
  - IP: `192.168.42.10`
  - hostname: `primary.gitlab`

The secondary Geo node has:
  - IP: `192.168.42.20`
  - hostname: `secondary.gitlab`

You can set the `root` password once you SSH into the machines using
`vagrant ssh <name>`. The `ubuntu` user is by default using passwordless sudo.

[vagrant]: https://www.vagrantup.com/downloads.html
[virtualbox]: https://www.virtualbox.org/wiki/Downloads
